﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Week1ContactApp
{
    [Serializable]
    public class AddressBook
    {

        private string FILE_SAVE_TYPE = "xml";
        private string FILE_NAME = "stuff";

        private List<Contact> ContactList;

        public AddressBook()
        {
            this.Load();
        }

        private void CreateNewList()
        {
            ContactList = new List<Contact>
            {
                new BusinessContact("Jason Shin", "0410344018", "WTF", "12093412094"),
                new PersonalContact("Alex", "0410696969", "FTW st Sydney"),
                new BusinessContact("Jacob", "129841920", "UTS", "124981294"),
                new PersonalContact("Luke the Nuker", "1294812904812904", "WHAT?! WHERE DOES HE LIVE?")
            };
        }

        public void Create(Contact contact)
        {
            ContactList.Add(contact);
        }

        public Contact Read(string name)
        {
            return ContactList.FirstOrDefault(x => x.Name == name);
        }

        public List<Contact> ReadAll()
        {
            return ContactList;
        }

        public void Delete(string name)
        {
            Contact foundContact = ContactList.First(x => x.Name == name);
            ContactList.Remove(foundContact);
        }

        public void Save()
        {
            using(var stream = File.OpenWrite(this.FILE_NAME))
            {
                var serialized = new XmlSerializer(typeof(List<Contact>), new[] { typeof(PersonalContact), typeof(BusinessContact) });
                serialized.Serialize(stream, this.ContactList);
            }
        }

        private void Load()
        {
            if(File.Exists(this.FILE_NAME))
            {
                using(var stream = File.OpenRead(this.FILE_NAME))
                {
                    var serialized = new XmlSerializer(typeof(List<Contact>), new[] { typeof(PersonalContact), typeof(BusinessContact) });
                    this.ContactList = (List<Contact>) serialized.Deserialize(stream);
                }
            } else
            {
                this.CreateNewList();
            }
        }
    }
}
