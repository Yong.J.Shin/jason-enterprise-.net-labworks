﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week1ContactApp
{
    class Program
    {
        private static AddressBook _AddressBook;
        
        static void Main(string[] args)
        {
            _AddressBook = new AddressBook();

            _AddressBook.Create(new BusinessContact("Larsen", "129831", "ATI", "1209312094"));
            PrintAll();
            _AddressBook.Save(); //Saving before program exits
            Console.ReadLine();
        }

        private static void PrintAll()
        {
            foreach(Contact currentContact in _AddressBook.ReadAll())
            {
                Console.WriteLine(currentContact);
            }
        }

    }
}
