﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week1ContactApp
{
    [Serializable]
    public class PersonalContact : Contact
    {
        public string Address { get; set; }

        public PersonalContact(string name, string phoneNumber, string address)
        {
            this.Name = name;
            this.PhoneNumber = phoneNumber;
            this.Address = address;
        }

        public PersonalContact()
        {

        }

        override
        public string ToString()
        {
            return String.Format(
                "Shit, this Personal Contact has follow values brah:\n"+
                "This person has this awesome name: {0}\n" +
                "And he has this phone number: {1}\n" +
                "Also he lives here so go and kill him: {2}\n\n"
                , this.Name, this.PhoneNumber, this.Address);
        }
    }
}
